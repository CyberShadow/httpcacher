import std.algorithm.searching;
import std.array;
import std.conv;
import std.exception;
import std.file;
import std.path;

import vibe.appmain;
import vibe.core.core;
import vibe.core.file;
import vibe.core.log;
import vibe.core.stream;
import vibe.http.client;
import vibe.http.fileserver;
import vibe.http.server;
import vibe.inet.message;

import ae.net.ietf.url : applyRelativeURL;
import ae.sys.file : ensurePathExists;

void startServer(
	string[] bindAddresses,
	ushort port,
	string[] cachePatterns,
	string proto,
)
{
	string cacheDir = expandTilde("~/data/software/os/Linux/package-cache");

	auto settings = new HTTPServerSettings;
	settings.port = port;
	settings.bindAddresses = bindAddresses;

	static immutable string[] non_forward_headers = [/*"Content-Length", */"Transfer-Encoding", "Content-Encoding", "Connection"];
	static InetHeaderMap non_forward_headers_map;
	if (non_forward_headers_map.length == 0)
		foreach (n; non_forward_headers)
			non_forward_headers_map[n] = "";

	listenHTTP(settings,
		(scope HTTPServerRequest req, scope HTTPServerResponse res)
		{
			auto resParts = req.path.split("/");
			enforce(resParts.length >= 3);
			enforce(resParts[0].length == 0);

			logInfo("%s", req.fullURL.toString());

			string cachePathForURL(string url)
			{
				string cachePath = (cacheDir.split("/") ~ url.split("/")[2..$]).join("/");
				if (cachePath.endsWith("/"))
					cachePath ~= "index.html";
				return cachePath;
			}

			void getFrom(string url)
			{
				auto cachePath = cachePathForURL(url);
				logInfo("Cache path: %s", cachePath);
				if (cachePath.exists && cachePath.isFile)
				{
					logInfo("Cache hit!");
					return sendFile(req, res, NativePath(cachePath));
				}

				logInfo("Cache miss.");
				try
					requestHTTP(url,
						(scope HTTPClientRequest creq) {
							creq.method = req.method;
							creq.headers = req.headers.dup;
							creq.headers["Host"] = resParts[1];
							req.bodyReader.pipe(creq.bodyWriter);
						},
						(scope HTTPClientResponse cres) {
							logInfo("%d %s", cres.statusCode, url);

							if (auto l = "Location" in cres.headers)
							{
								auto targetURL = applyRelativeURL(url, *l);
								getFrom(targetURL);
								auto targetPath = cachePathForURL(targetURL);
								ensurePathExists(cachePath);
								symlink(relativePath(targetPath, cachePath.dirName), cachePath);
								return;
							}

							res.statusCode = cres.statusCode;

							// copy all headers that may pass from upstream to client
							foreach (key, ref value; cres.headers.byKeyValue)
								if (key !in non_forward_headers_map)
									res.headers[key] = value;

							if (res.isHeadResponse)
								res.writeVoidBody();
							else if (res.statusCode == HTTPStatus.ok &&
								cachePatterns.any!(pattern => req.path.canFind(pattern)))
							{
								auto tmpPath = cachePath ~ ".partial";

								// auto output = res.bodyWriter;

								ensurePathExists(cachePath);

								cres.bodyReader.pipe(tee(
									res.bodyWriter,
									openFile(tmpPath, FileMode.createTrunc)
								));

								rename(tmpPath, cachePath);
							}
							else
								cres.bodyReader.pipe(res.bodyWriter);
						}
					);
				catch (Exception e)
					throw new HTTPStatusException(HTTPStatus.badGateway, "Connection to upstream server failed: "~e.msg);
			}

			getFrom(proto ~ "://" ~ resParts[1..$].join("/"));
		});
}

shared static this()
{
	// Debian
	startServer(
		["127.116.113.85"],
		18006,
		[
			"/pool/",
			"/by-hash/",
		],
		"http",
	);

	// Arch Linux, for Docker
	startServer(
		["172.17.0.1"],
		18007,
		["/"],
		"https",
	);
}

class Tee(Outputs...) : OutputStream
{
	Outputs children;
	this(Outputs children) { this.children = children; }

	size_t write(in ubyte[] bytes, IOMode mode)
	{
		foreach (child; children)
		{
			const(ubyte)[] childBytes = bytes;
			while (childBytes.length)
			{
				auto written = child.write(childBytes, mode);
				childBytes = childBytes[written..$];
			}
		}
		return bytes.length;
	}

	void flush()
	{
		foreach (child; children)
			child.flush();
	}

	void finalize()
	{
		foreach (child; children)
			child.finalize();
	}
}

OutputStream tee(Outputs...)(Outputs outputs)
{
	return new Tee!Outputs(outputs);
}
